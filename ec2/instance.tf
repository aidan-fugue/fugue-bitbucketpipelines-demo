// EC2 INSTANCE(S)

// LnL t2.micro
resource "aws_instance" "app-server-b-LnL-26Jan22" {
  ami           = "ami-066333d9c572b0680"
  instance_type = "t2.micro"

  
  // Un-comment below to satisfy FG_R00253
  #iam_instance_profile = aws_iam_instance_profile.test_profile.name
  
  // Comment below to satisfy FG_R00271
  # Make the below declaration = false to satisfy FG_R00271
  associate_public_ip_address = true

  tags = {
    Name = "app-server-b-LnL-26Jan22"
    Team = "test"
  }
}

// t2.micro instance

resource "aws_instance" "app-server-a" {
  ami           = "ami-066333d9c572b0680"
  instance_type = "t2.micro"

  
  // Un-comment below to satisfy FG_R00253
  #iam_instance_profile = aws_iam_instance_profile.test_profile.name
  
  // Comment below to satisfy FG_R00271
  # Make the below declaration = false to satisfy FG_R00271
  associate_public_ip_address = true

  tags = {
    Name = "app-server-lnl-26Feb22"
    Team = "dev"
  }
}

// IAM INSTANCE PROFILE(S)

resource "aws_iam_instance_profile" "test_profile" {
  name = "test_profile"
  role = aws_iam_role.role.name
}

resource "aws_iam_role" "role" {
  name = "test_role"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}
